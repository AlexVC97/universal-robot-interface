#!/usr/bin/env python

from socket import *
import binascii

IP = '192.168.0.181'
PORT = 9100
MESSAGE1 = "CLS\n"
MESSAGE2 = "TEXT 400,26, \"2\",0,1,1,2,\"TELEPEN Number\"\n"
MESSAGE3 = "BARCODE 400,50, \"TELEPENN\",60,2,0,2,6,2, \"1234567890\"\n"
MESSAGE4 = "PRINT 1\n"

s = socket(AF_INET, SOCK_STREAM)
s.settimeout(3)
s.connect((IP, PORT))
s.send(MESSAGE1)
s.send(MESSAGE2)
s.send(MESSAGE3)
s.send(MESSAGE4)

s.close()
