#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import sys

# Import classes
from lib.config_handler import ConfigHandler
from lib.log_handler import LogHandler
from lib.broadcast import Broadcast
from lib.client_handler import ClientHandler
from lib.thread_handler import ThreadHandler

def main():
    configHandler = ConfigHandler()
    logHandler = LogHandler("main.pepper.out")
    threadHandler = ThreadHandler(logHandler)

    if(configHandler.getSettingByKey("tcp_receiver") == True):
        logHandler.addSocket(configHandler.getSettingByKey("tcp_receiver_ip"))
    
    threadHandler.startThread("nfc")
    threadHandler.startThread("scale")
    threadHandler.startThread("barcode")

    interrupt = raw_input("Enter \"q\" to quit the program...\n")

    while interrupt == "q":
        threadHandler.stopThread("nfc")
        threadHandler.stopThread("scale")
        threadHandler.stopThread("barcode")
        sys.exit(1)

    sys.exit(0)

if __name__ == "__main__":
    main()
