#!/usr/bin/env python

from socket import *
from time import *
import re
import logging

class Broadcast():
    """
    The purpose of the Broadcast class is to send a broadcast message on the
    subnet. The reason for this is to find the IP address of the robot. The
    broadcast message contains a serial number and this serial number needs to
    match with the one that the robot has.
    """

    def __init__(self, serialNo, logger):
        """
        The constructor needs 2 additional parameters. It needs serialNo, this
        stands for the serial number of Pepper. It also needs logger, this stands
        for an instance of logging that has been made in the main.py.
        """
        self.serialNo = serialNo
        self.data = ""
        self.address = ("", 5001)
        self.validIpAddressRegex = r'(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})'
        self.udpSocket = socket(AF_INET, SOCK_DGRAM)
        self.ip = [{""}]
        self.logger = logger
        self.config_socket()
        self.send_broadcast()

    def config_socket(self):
        """
        This method is used to create a socket and needs to be called first. This
        method doesn't need any arguments to be given.
        """
        # Indicates if the local address can be reused
        self.udpSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        # Set TTL in the IP header
        self.udpSocket.setsockopt(SOL_IP, IP_TTL, 1)
        # Enable the socket for issuing messages to a broadcast address
        self.udpSocket.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        self.udpSocket.bind(self.address)
        self.udpSocket.settimeout(1)
        print "Socket has been configured, waiting for the IP address.."
        self.logger.logInfo("broadcast:Socket has been configured, waiting for the IP address..")

    def send_broadcast(self):
        """
        This method is used to send the broadcast message and to receive the
        IP address of the robot. This method doesn't need any arguments to be
        given.
        """
        while(self.data != "true"):
            try:
                self.udpSocket.sendto(self.serialNo, ("<broadcast>", 5000))
                self.data, addr = self.udpSocket.recvfrom(1024) # 1 kilo Byte
            except error:
                self.data = None
            if self.data is None:
                print "Nothing received yet! Try again!"
                sleep(3)

        """
        After doing this you will get the IP address together with the port
        number and this is the reason why my validation didn't succeeded when the
        4th number of the IP address didn't had three digits.
        """
        #convert = "".join(map(str,addr)

        self.ip = re.findall(self.validIpAddressRegex, str(addr))
        print "Received: " + self.ip[0]
        self.logger.logInfo("broadcast:Received: " + self.ip[0])
