#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import paho.mqtt.client as mqtt
from time import *
import logging

# Import classes
from config_handler import ConfigHandler
from communication import Communication

class ClientHandler():
    """
    The purpose of the ClientHandler class is to bind the so called callback
    functions from the Communication class and to make a connection to the
    MQTT broker.
    """

    def __init__(self, broker_client, broker_ip, broker_port, logger):
        """
        The constructor needs 4 additional parameters. It needs the broker_ip, this
        stands for the IP address of the MQTT broker that we want to use.
        It also needs the broker_port, this stands for the port number to make the
        connection. It also needs config and logger, both are instances, one of
        ConfigHandler and the one of logging,both are made in the main.py.
        """
        self.broker_client = broker_client
        self.broker_ip = broker_ip
        self.broker_port = broker_port
        self.config = ConfigHandler()
        self.logger = logger
        self.communication = Communication(logger)
        self.client = mqtt.Client(self.broker_client, True, None, mqtt.MQTTv31) # Create new instance
        self.bind_callbacks()
        self.make_connection()

    def bind_callbacks(self):
        """
        This method is used to create links to the callback functions. Without this,
        the methods from the Communication class will not be called.
        """
        self.client.username_pw_set(username=self.config.getSettingByKey("mqtt_username"), password=self.config.getSettingByKey("mqtt_password"))
        mqtt.Client.connected_flag = False # Create flag in class
        self.client.on_connect = self.communication.on_connect
        self.client.on_disconnect = self.communication.on_disconnect
        self.client.on_publish = self.communication.on_publish

    def make_connection(self):
        """
        This method is used to establish a connection with the MQTT broker.
        """
        try:
            # Establish connection
            self.client.connect(self.broker_ip, self.broker_port)
        except Exception:
            print "Connection can't be established with " + str(self.broker_ip)
            self.logger.logWarning("clienthandler:Connection can't be established with ", str(self.broker_ip))
        self.client.loop_start()
        self.logger.logInfo("clienthandler:Waiting for connection..")
        while not self.client.connected_flag: # Wait in loop
            print "Waiting for connection.."
            sleep(1)
        sleep(3)

    def publishing(self, topic, msg):
        """
        This method is used to handle all the publishes that we need to make.
        """
        ret = self.client.publish(topic, msg, 0) # Publish
        # We don't need to know this everytime
        #print "Published return = " + str(ret)

    def disconnect(self):
        """
        This method is used to disconnect the MQTT broker.
        """
        self.client.loop_stop()
        self.client.disconnect()
