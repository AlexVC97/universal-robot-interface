#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import usb.core
import usb.util
import logging
import logging.handlers
import time
import json
import datetime

# Import classes
from config_handler import ConfigHandler
from log_handler import LogHandler
from connection import Connection

class ElaneScaleUsb():
    """
    The purpose of the UsbScale class is to get the weight value from the scale.
    But the value only appears when it is stable. Then we need to publish
    the weight on a MQTT topic. This is done with a json object because we want
    some additional information.
    """

    def __init__(self):
        """
        The constructor needs 3 additional parameters. It needs client, config and
        logger, these are instances, the first one of ClientHandler, the second one of
        ConfigHandler and the third one of logging. These instances were all made
        in the main.py.
        """
        # Find the USB device
        self.vid = 0x7b7c
        self.pid = 0x0201
        self.config = ConfigHandler()
        self.logger = LogHandler("scale.pepper.out")
        self.connection = Connection("Scale_MQTT", self.logger)
        self.json_data = ""
        self.pkt_ctr = 1
        self.weight = ""
        self.units = ""
        self.running = True
        self.interface = 0
    
    def make_json(self):
        data = {"weight": 0, "units": "gr", "pkt_ctr": 0}
        epoch = int(datetime.datetime.now().strftime("%s"))
        data['pkt_ctr'] = self.pkt_ctr
        data['weight'] = self.weight
        data['units'] = self.units
        data['timestamp'] = epoch
        self.json_data = json.dumps(data)
    
    def data_publish(self):
        try:
            self.connection.publish(self.config.getSettingByKey("scale_topic"), self.json_data)
        except Exception:
            self.logger.logWarning("scale:Couldn't publish the data!")

    def stop(self):
        """
        This method is used to stop the Thread. But when the project has been
        installed and it is running on boot, you can ignore this method.
        """
        self.running = False

    def main(self):
        DATA_MODE_GRAMS = 2
        DATA_MODE_OUNCES = 11
        oldWeight = 0
        check_1 = 0
        check_2 = 0
        check_3 = 0
        check_4 = 0

        while self.running:
            while check_1 == 0:
                try:
                    self.dev = usb.core.find(idVendor = self.vid, idProduct = self.pid)
                    # First endpoint
                    endpoint = self.dev[0][(0,0)][0]
                    check_2 = 1
                    check_1 = 1
                except:
                    print "Waiting until device is found..."
                    check_4 = 1
                    check_3 = 0
                    check_2 = 0
                    time.sleep(1)

            while check_2 == 1:
                try:
                    # If the OS kernel already claimed the device, which is most likely true
                    if self.dev.is_kernel_driver_active(self.interface) is True:
                        # Tell the kernel to detach
                        self.dev.detach_kernel_driver(self.interface)
                        # Claim the device
                        usb.util.claim_interface(self.dev, self.interface)
                        check_2 = 0
                        check_3 = 1
                except:
                    print "The device wasn't attached to the kernel.\n"
                    time.sleep(1)
                    check_1 = 0
                    check_3 = 0
                    check_2 = 0

            while check_3 == 1:
                try:
                    data = self.dev.read(endpoint.bEndpointAddress, endpoint.wMaxPacketSize)

                    if data != None:
                        raw_weight = data[4] + data[5] * 256

                    if data[3] == 255:
                        scaling_factor = 10 ** (data[3] - 256)

                    if data[2] == DATA_MODE_GRAMS:
                        if data[1] == 5:
                            grams = raw_weight
                            self.weight = grams - 2**16
                            self.units = "gr"
                        else:
                            grams = raw_weight
                            self.weight = "%s" % grams
                            self.units = "gr"
                    elif data[2] == DATA_MODE_OUNCES:
                        if data[1] == 5:
                            ounces = raw_weight * scaling_factor
                            self.weight = ounces - 2**16 * scaling_factor
                            self.units = "oz"
                        else:
                            ounces = raw_weight * scaling_factor
                            self.weight = "%s" % ounces
                            self.units = "oz"

                    if oldWeight != self.weight or check_4 == 1:
                        oldWeight = self.weight
                        print self.weight + " " + self.units
                        self.logger.logInfo("scale:Weight received: ", self.weight)
                        self.make_json()
                        self.data_publish()
                        self.pkt_ctr += 1
                        check_4 = 0
                except usb.core.USBError:
                    data = None
                    check_1 = 0
                    check_3 = 0

if __name__ == "__main__":
    ob = ElaneScaleUsb()
    ob.main()
