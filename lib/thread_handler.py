#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries

# Import classes
from config_handler import ConfigHandler
from log_handler import LogHandler
from lib.nfc import Nfc
from lib.scale import Scale
from lib.barcode import Barcode

class ThreadHandler():
    def __init__(self, logger):
        self.config = ConfigHandler()
        self.logger = logger

    def startThread(self, threadName):
        if(threadName == "nfc"):
            if(self.config.getSettingByKey("nfc") == True):
                self.nfcThread = Nfc()
                self.nfcThread.start()
                # Welcome message
                print "The NFC Thread has been started."
                self.logger.logInfo("main:The NFC Thread has been started.")
            else:
                self.logger.logInfo("main:The NFC Thread has not been enabled!")
        elif(threadName == "scale"):
            if(self.config.getSettingByKey("elane_scale_usb5") == True):
                self.usbScaleThread = Scale()
                self.usbScaleThread.start()
                # Welcome message
                print "The SCALE_ELANE_USB5 Thread has been started."
                self.logger.logInfo("main:The SCALE_ELANE_USB5 Thread has been started.")
            else:
                self.logger.logInfo("main:The SCALE_ELANE_USB5 Thread has not been enabled!")
        elif(threadName == "barcode"):
            if(self.config.getSettingByKey("barcode") == True):
                self.barcodeThread = Barcode()
                self.barcodeThread.start()
                # Welcome message
                print "The BARCODE Thread has been started."
                self.logger.logInfo("main:The BARCODE Thread has been started.")
            else:
                self.logger.logInfo("main:The BARCODE Thread has not been enabled!")
        else:
            print "The device doesn't exist! Try again later.."
            self.logger.logInfo("main:The device " + threadName + " doesn't exist! Try again later..")
    
    def stopThread(self, threadName):
        if(threadName == "nfc"):
            if(self.config.getSettingByKey("nfc") == True):
                self.nfcThread.stop()
                self.nfcThread.join()
        elif(threadName == "scale"):
            if(self.config.getSettingByKey("elane_scale_usb5") == True):
                self.usbScaleThread.stop()
                self.usbScaleThread.join()
        elif(threadName == "barcode"):
            if(self.config.getSettingByKey("barcode") == True):
                self.barcodeThread.stop()
                self.barcodeThread.join()
        else:
            print "The device doesn't exist! Try again later.."
            self.logger.logInfo("main:The device " + threadName + " doesn't exist! Try again later..")