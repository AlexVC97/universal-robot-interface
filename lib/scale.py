#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
from threading import Thread

# Import classes
from elane_scale_usb import ElaneScaleUsb

class Scale(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.scaleObj = ElaneScaleUsb()

    def run(self):
        """
        Because we are inheriting from Thread (to make it run independently) we need
        to have this specific called method "run()" in our class. This needs to
        contain every behavior of your Thread. You can start the Thread with
        <thread_name>.start() and this will start the run() method.
        """
        self.scaleObj.main()
    
    def stop(self):
        self.scaleObj.stop()