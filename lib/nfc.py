#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
from threading import Thread

# Import classes
from nfc_reader import NfcReader

class Nfc(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.nfcObj = NfcReader()
    
    def run(self):
        self.nfcObj.main()
    
    def stop(self):
        self.nfcObj.stop()