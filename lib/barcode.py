#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
from threading import Thread

# Import classes
from barcode_reader import BarcodeReader

class Barcode(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.barcodeObj = BarcodeReader()
    
    def run(self):
        self.barcodeObj.main()
    
    def stop(self):
        self.barcodeObj.stop()