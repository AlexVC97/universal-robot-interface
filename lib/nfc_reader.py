#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import RPi.GPIO as GPIO
import MFRC522
import json
import datetime

# Import classes
from config_handler import ConfigHandler
from log_handler import LogHandler
from connection import Connection

class NfcReader():
    """
    The purpose of the Nfc class is to get the NFC tag when it has been
    detected by the NFC reader. Then we need to publish the uid on a MQTT topic.
    This is done with a json object because we want some additional information.
    """

    def __init__(self):
        """
        The constructor needs 3 additional parameters. It needs client, config and
        logger, these are instances, the first one of ClientHandler, the second one of
        ConfigHandler and the third one of logging. These instances were all made
        in the main.py.
        """
        self.config = ConfigHandler()
        self.logger = LogHandler("nfc.pepper.out")
        self.connection = Connection("Nfc_MQTT", self.logger)
        self.json_data = ""
        self.pkt_ctr = 1
        self.uid = ""
        self.running = True

    def make_json(self):
        data = {"uid": [0,0,0,0,0], "pkt_ctr": 0}
        epoch = int(datetime.datetime.now().strftime("%s"))
        data['pkt_ctr'] = self.pkt_ctr
        data['uid'] = str(self.uid)
        data['timestamp'] = epoch
        self.json_data = json.dumps(data)
            
    def data_publish(self):
        try:
            self.connection.publish(self.config.getSettingByKey("nfc_topic"), self.json_data)
        except Exception:
            self.logger.logWarning("nfc:Couldn't publish the data!")

    def stop(self):
        """
        This method is used to stop the Thread. But when the project has been
        installed and it is running on boot, you can ignore this method.
        """
        self.running = False

    def main(self):
        """
        Because we are inheriting from Thread (to make it run independently) we need
        to have this specific called method "run()" in our class. This needs to
        contain every behavior of your Thread. You can start the Thread with
        <thread_name>.start() and this will start the run() method.
        """
        oldUid = [0,0,0,0,0]

        # Create an object of the class MFRC522
        MIFAREReader = MFRC522.MFRC522()
        MIFAREReader.Write_MFRC522(0x04,0x55)

        # This loop keeps checking for chips. If one is near it will get the UID and authenticate
        while self.running:
            # Scan for cards
            (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

            #sleep(1)
            # If a card is found
            if status == MIFAREReader.MI_OK:
                #print "Card detected"
                # Get the UID of the card
                (status, self.uid) = MIFAREReader.MFRC522_Anticoll()

                # If we have the UID, continue
                if status == MIFAREReader.MI_OK:
                    if oldUid != self.uid:
                        oldUid = self.uid
                        print "Read: " + str(self.uid)
                        self.logger.logInfo("nfc:Card read UID: ", str(self.uid))
                        self.make_json()
                        self.data_publish()
                        self.pkt_ctr += 1
        
if __name__ == "__main__":
    #logger = LogHandler("nfc.pepper.out")
    #ob = NfcReader(logger)
    ob = NfcReader()
    ob.main()