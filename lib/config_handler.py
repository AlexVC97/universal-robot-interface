#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import json

class ConfigHandler():
    """
    The purpose of the ConfigHandler class is to retrieve all the configurations
    that were placed in the config.json file.
    """

    def __init__(self):
        """
        The constructor doesn't need any additional parameters.
        """
        self.data = ""
        self.settings = {}
        self.read_json()

    def read_json(self):
        """
        This method is used to read all the configurations from the config.json
        configuration file.
        """
        with open('/home/pi/universal-robot-interface/config.json') as json_data:
            self.data = json.load(json_data)
            self.settings["tcp_receiver"] = self.check_data('TCP_RECEIVER')
            self.settings["tcp_receiver_ip"] = self.check_data('TCP_RECEIVER_IP')
            self.settings["auto_broker_detection"] = self.check_data('AUTO_BROKER_DETECTION')
            self.settings["robot_serial_no"] = self.check_data('ROBOT_SERIAL_NO')
            self.settings["mqtt_broker"] = self.check_data('MQTT_BROKER')
            self.settings["mqtt_port"] = self.check_data('MQTT_PORT')
            self.settings["mqtt_client"] = self.check_data('MQTT_CLIENT')
            self.settings["mqtt_username"] = self.check_data('MQTT_USERNAME')
            self.settings["mqtt_password"] = self.check_data('MQTT_PASSWORD')
            self.settings["nfc"] = self.check_data('NFC')
            self.settings["nfc_topic"] = self.check_data('NFC_TOPIC')
            self.settings["elane_scale_usb5"] = self.check_data('ELANE_SCALE_USB5')
            self.settings["scale_topic"] = self.check_data('SCALE_TOPIC')
            self.settings["barcode"] = self.check_data('BARCODE')
            self.settings["barcode_topic"] = self.check_data('BARCODE_TOPIC')
    
    def getSettingByKey(self, key):
        return self.settings.get(key)

    def check_data(self, setting):
        try:
            return self.data[setting]
        except:
            return False

