#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import logging
import logging.handlers

class LogHandler():
    def __init__(self, filename):
        self.confLogger(filename)
    
    def confLogger(self, filename):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
        rotate_handler = logging.handlers.RotatingFileHandler(filename, mode='a', maxBytes=1024000, backupCount=5)
        rotate_handler.setFormatter(formatter)
        self.logger.addHandler(rotate_handler)

    def addSocket(self, keyName):
        socket_handler = logging.handlers.SocketHandler(keyName, 1887)
        self.logger.addHandler(socket_handler)
    
    def logInfo(self, message, value=""):
        self.logger.info(message + value)
    
    def logWarning(self, message, value=""):
        self.logger.warning(message)