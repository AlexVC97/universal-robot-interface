#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
from time import sleep
from serial import Serial, SerialException, time
import serial
import serial.tools.list_ports
import platform
import json
import datetime

# Import classes
from config_handler import ConfigHandler
from log_handler import LogHandler
from connection import Connection

HONEY_WELL_ID = 'VID:PID=0C2E:0B6A'
HONEY_WELL_BAUD = 115200

class BarcodeReader():
    def __init__(self):
        self.vid = 0x0C2E
        self.pid = 0x0B61
        self.config = ConfigHandler()
        self.logger = LogHandler("barcode.pepper.out")
        self.connection = Connection("Barcode_MQTT", self.logger)
        self.json_data = ""
        #self.pkt_ctr = 1
        self.running = True

    def make_json(self):
        data = {"boardingData": "", "timestamp": ""}
        epoch = int(datetime.datetime.now().strftime("%s"))
        #data['pkt_ctr'] = self.pkt_ctr
        data['boardingData'] = str(self.data)
        data['timestamp'] = epoch
        self.json_data = json.dumps(data)
    
    def data_publish(self):
        try:
            self.connection.publish(self.config.getSettingByKey("barcode_topic"), self.json_data)
        except Exception:
            self.logger.logWarning("barcode:Couldn't publish the data!")

    def find_device(self, ID):
        comports = serial.tools.list_ports.comports()
        for port in comports:
            if platform.system() == 'Windows':
                if ID.upper() in port.hwid.upper():
                    print "Found device at: " + port.device
            else:
                if ID.upper() in port[2].upper():
                    print "Found device at: " + port[0]
                    return port[0]

    def handle_barcodes(self, data):
        self.data = data
        print "Read barcode: " + self.data
        self.make_json()
        self.data_publish()
    
    def stop(self):
        """
        This method is used to stop the Thread. But when the project has been
        installed and it is running on boot, you can ignore this method.
        """
        self.running = False

    def main(self):
        while True:
            bar = None
            print "Searching for device"
            device = self.find_device(HONEY_WELL_ID)
            if device:
                print "Device found at: " + str(device)
                try:
                    bar = Serial(device, baudrate=HONEY_WELL_BAUD)
                except ValueError:
                    print "Parameters are incorrect! Try again.."
                except SerialException:
                    print "Device not found, can't configure device.."
                
                if bar:
                    while True:
                        try:
                            print "Waiting for barcode.."
                            self.handle_barcodes(bar.readline())
                        except SerialException as e:
                            print "Error while reading: " + str(e)
                            bar.close()
                            break
                else:
                    time.sleep(1)
                    continue # Exception occurred, bar not initialised
            else:
                print "Device not found!"
                time.sleep(1)
                continue # Try until device is connected, waiting on usb connected events is to platform specific

if __name__ == "__main__":
    ob = BarcodeReader()
    ob.main()