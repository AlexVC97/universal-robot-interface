#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries
import time
import logging

class Communication():
    """
    The purpose of the Communication class is to create our so called
    callback functions.
    """

    def __init__(self, logger):
        """
        The constructor needs 1 additional parameter. It needs logger, this stands
        for an instance of logging that has been made in the main.py.
        """
        self.logger = logger

    def on_connect(self, client, userdata, flags, rc):
        """
        The connection acknowledged event will trigger this callback.
        """
        if rc == 0:
            client.connected_flag = True # Set flag
            self.logger.logInfo("communication:Client connected OK")
        elif rc == 5:
            self.logger.logInfo("communication:Authentication failed returned code = ", str(rc))
        else:
            self.logger.logInfo("communication:Bad connection returned code = ", str(rc))
            client.loop_stop()

    def on_publish(self, client, userdata, mid):
        """
        The publish acknowledged event will trigger this callback.
        """
        self.logger.logInfo("communication:In on_publish callback mid = ", str(mid))

    def on_disconnect(self, client, userdata, rc):
        """
        The disconnection acknowledged event will trigger this callback.
        """
        self.logger.logInfo("communication:Client disconnected OK")
