"""
The __init__.py file is required to make Python treat the directories as
containing packages. This is done to prevent directories with a common name,
such as string, from unintentionally hiding valid modules that occur later
(deeper) on the module search path.
"""
