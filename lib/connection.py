#!/usr/bin/env python
# -*- coding: utf8 -*-

# Import libraries

# Import classes
from config_handler import ConfigHandler
from broadcast import Broadcast
from client_handler import ClientHandler

class Connection():
    def __init__(self, clientName, logger):
        self.clientName = clientName
        self.config = ConfigHandler()
        self.logger = logger
        self.getMqttBroker()

    def getMqttBroker(self):
        if(self.config.getSettingByKey("auto_broker_detection") == True):
            broadcast = Broadcast(self.config.getSettingByKey("robot_serial_no"), self.logger)
            self.clientHandler = ClientHandler(self.clientName, broadcast.ip[0], self.config.getSettingByKey("mqtt_port"), self.logger)
        else:
            self.clientHandler = ClientHandler(self.clientName, self.config.getSettingByKey("mqtt_broker"),
                self.config.getSettingByKey("mqtt_port"), self.logger)
        
    def publish(self, topic, message):
        self.clientHandler.publishing(topic, message)
